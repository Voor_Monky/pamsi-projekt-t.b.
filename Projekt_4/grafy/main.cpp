#include <fstream>
#include <numeric>
#include <stdlib.h>

#include "inc/adjacencyList.hpp"
#include "inc/adjacencyMatrix.hpp"
#include "src/Dijkstra.cpp"
#include "src/Bellman-Ford.cpp"
#include "../tester/include/spdlog/spdlog.h"
#include "../tester/include/pamsi_tester/Tester.hpp"

//std::ostream& operator<< (std::ostream& strm, std::vector<std::vector<int> >& vect)   //moved to Tester.hpp
//{
//    strm << vect.at(0).at(0) << std::endl;
//
//    for(unsigned int i = 1; i < vect.size(); ++i)
//    {
//        for(unsigned int j = 0; j < vect.at(i).size(); ++j)
//        {
//            strm << vect.at(i).at(j) << ' ';
//        }
//        strm << '\t';
//        if(i % 2 == 0)
//            {strm << std::endl;}
//    }
//
//    return strm;
//}

template<typename graphType>    //szablon moze przyjac jeden z typow grafow
graphType makeRandGraph (unsigned int vertNum, unsigned int edgeNum)
{
    std::srand(std::time(0));
    graphType result;
    unsigned int ind1 = 0, ind2 = 0;

    if(edgeNum > (vertNum * (vertNum - 1) * 0.5) )
    {
        std::cerr << "Wrong number of edges for RandGraph!" << std::endl;
        return result;
    }

    for(unsigned int i = 0; i < vertNum; ++i)
        {result.insertVertice(std::rand() % 100);}

    ind1 = std::rand() % vertNum;
    for(unsigned int i = 0, j = 0; i < edgeNum; ++i)
    {
        ind2 = (ind1 + 1 + i) % vertNum;

        if( (ind1 - j) % vertNum == ind2)
        {
            ++ind1;
            ind2 = (ind1 + 1) % vertNum;
            ++j;
        }

        result.insertEdge(&(result.retVertices()->at(ind1) ), &(result.retVertices()->at(ind2) ), std::rand() % 100);
    }

    return result;
}

template<typename graphType>
using FunType = std::vector<std::vector<int> > (*)(graphType);

template<typename graphType, FunType<graphType> algorithm>
class GraphTester : public Tester<std::vector<std::vector<int> >, graphType>
{
  protected:
    std::vector<std::vector<int> > runAlgorithm(const graphType& inputData) override;
    graphType readSingleInput(std::istream& inputStream) override;
};

template<typename graphType, FunType<graphType> algorithm>
std::vector<std::vector<int> > GraphTester<graphType, algorithm>::runAlgorithm(const graphType& inputData)
{
    return algorithm(inputData);
}

template<typename graphType, FunType<graphType> algorithm>
graphType GraphTester<graphType, algorithm>::readSingleInput(std::istream& inputStream)
{
    graphType result;

    inputStream >> result;

    return result;
}

int main()
{
    std::fstream inputFile{"../grafy/input.txt"};
    std::ofstream dataOutputFile{"../grafy/output.txt"}, timeOutputFile{"../grafy/times.csv"};//, makeInput{"../grafy/input.txt"};

    //GraphTester<adjList, dijkstra<adjList> > tester;
    //GraphTester<adjList, bellford<adjList> > tester;
    //GraphTester<adjMatrix, dijkstra<adjMatrix> > tester;
    //GraphTester<adjMatrix, bellford<adjMatrix> > tester;

    if(!inputFile)
    {
        spdlog::error("input.txt cannot be opened!");
        return -1;
    }

    //tester.runAllTests(inputFile, dataOutputFile, timeOutputFile);

    /**Tworzenie pliku wejsciowego z wykorzystaniem dowolnego typu grafu**/
    //adjMatrix tmp1 = makeRandGraph<adjMatrix>(200, 19900);   //1. arg. - l. wierzcholkow, 2. arg. - l. krawedzi
    //adjList tmp2 = makeRandGraph<adjList>(10,10);
    //inputFile << tmp1;

    /**Testy algorytmów**/
    //adjMatrix file; inputFile >> file; std::vector<std::vector<int> > tmp = dijkstra<adjMatrix>(file); dataOutputFile << tmp;     //Dijkstra dla macierzy sasiedztwa
    //adjList file; inputFile >> file; std::vector<std::vector<int> > tmp = dijkstra<adjList>(file); dataOutputFile << tmp;       //Dijkstra dla listy sasiedztwa
    //adjMatrix file; inputFile >> file; std::vector<std::vector<int> > tmp = bellford<adjMatrix>(file); dataOutputFile << tmp;     //Bellman-Ford dla macierzy sasiedztwa
    //adjList file; inputFile >> file; std::vector<std::vector<int> > tmp = bellford<adjList>(file); dataOutputFile << tmp;       //Bellman-Ford dla listy sasiedztwa
}