#ifndef ADJACENCY_MATRIX
#define ADJACENCY_MATRIX

#include "vertice&edge.hpp"

class adjMatrix
{
    std::vector<mtxVertice> verticeList;
    std::vector<mtxEdge> edgeList;
    std::vector<std::vector<mtxEdge*> > adjMtx;

 public:
    adjMatrix ();
    adjMatrix (const adjMatrix& graph) {this->verticeList = graph.verticeList; this->edgeList = graph.edgeList; this->adjMtx = graph.adjMtx;}
    ~adjMatrix () {}
    std::vector<mtxVertice*> endVertices (mtxEdge* /*edge*/);
    mtxVertice* opposite (mtxVertice* /*vert*/, mtxEdge* /*edge*/);
    bool areAdjacent (mtxVertice* /*vert1*/, mtxVertice* /*vert2*/);
    void replace (mtxVertice* /*vert*/, int /*elem*/);
    void replace (mtxEdge* /*edge*/, int /*value*/);
    void insertVertice (int /*elem*/);
    void insertEdge (mtxVertice* /*start*/, mtxVertice* /*end*/, int /*elem*/);
    void removeVertice (mtxVertice* /*vert*/);
    void removeEdge (mtxEdge* /*edge*/);
    std::vector<mtxEdge*> incidentEdges (mtxVertice* /*vert*/);
    std::vector<mtxVertice>* retVertices ();
    std::vector<mtxEdge>* retEdges ();
};

std::istream& operator>> (std::istream& /*strm*/, adjMatrix& /*graph*/);
std::ostream& operator<< (std::ostream& /*strm*/, adjMatrix& /*graph*/);

#endif //ADJACENCY_MATRIX