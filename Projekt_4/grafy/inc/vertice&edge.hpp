#ifndef EDGE_LIST
#define EDGE_LIST

#include <iostream>
#include <vector>
#include <stdlib.h>

class vertice
{
    int elem;
    vertice* onList;

 public:
     vertice (): elem(0), onList(nullptr) {}
     vertice (const vertice& vert) {this->elem = vert.elem; this->onList = new vertice; *(this->onList) = *(vert.onList);}
     ~vertice () {delete this->onList;}
     //vertice& operator= (const vertice vrt) {delete this->onList; this->elem = vrt.retElem(); this->onList = vrt.retOnList(); return *this;}
     vertice& operator= (const int value) {this->elem = value; return *this;}
     vertice& operator= (const vertice* ptr) {*(this->onList) = *(ptr); return *this;}
     int retElem () const {return this->elem;}
     vertice* retOnList () const {return this->onList;}
};

//class edge
//{
//    int value = 0;
//    vertice* startVert = nullptr;
//    vertice* endVert = nullptr;
//    edge* onList = nullptr;
//
// public:
//     edge () {}
//     edge (vertice* stVt, vertice* edVt) {*(this->startVert) = *(stVt); *(this->endVert) = *(edVt);}
//     edge (const edge& edg) {this-> value = edg.value;
//                              this->startVert = new vertice; *(this->startVert) = *(edg.startVert);
//                              this->endVert = new vertice; *(this->endVert) = *(edg.endVert);
//                              this->onList = new edge; *(this->onList) = *(edg.onList);}
//     ~edge () {delete this->startVert; delete this->endVert; delete this->onList;}
//     edge& operator= (const edge edg) {delete this->startVert; delete this->endVert; delete this->onList;
//                                       this->value = edg.retValue();
//                                       this->startVert = edg.retStart();
//                                       this->endVert = edg.retEnd();
//                                       this->onList = edg.retOnList(); return *this;}
//     edge& operator= (const int val) {this->value = val; return *this;}
//     void writeOnList (edge* ptr) {*(this->onList) = *(ptr);}
//     int retValue () const {return this->value;}
//     vertice* retStart () const {return this->startVert;}
//     vertice* retEnd () const {return this->endVert;}
//     edge* retOnList () const {return this->onList;}
//};

/****************************** Klasy dla listy sasiedztwa. ******************************/

class listEdge;

class listVertice
{
    int index;
    int elem;
    listVertice* onList;
    std::vector<listEdge*> edges;

 public:
     listVertice (): index(0), elem(0), onList(nullptr), edges() {}
     listVertice (int elem): index(0), elem(elem), onList(nullptr), edges() {}
     listVertice (const listVertice& vert) {this->index = vert.index; this->elem = vert.elem; this->onList = new listVertice; this->onList = vert.onList; this->edges = vert.edges;}
     ~listVertice () {delete this->onList;}
     listVertice& operator= (listVertice vrt) {delete this->onList; this->edges.clear();
                                               this->index = vrt.retIndex(); this->elem = vrt.retElem(); this->onList = vrt.retOnList(); this->edges = vrt.getEdges(); return *this;}
     listVertice& operator= (const int value) {this->elem = value; return *this;}
     listVertice& operator= (const listVertice* ptr) {*(this->onList) = *(ptr); return *this;}
     bool operator== (listVertice vrt) {if(this->index == vrt.index) return true; else return false;}
     //void addEdge (listEdge* newEd);
     int& retIndex () {return this->index;}
     int retElem () const {return this->elem;}
     listVertice* retOnList () const {return this->onList;}
     std::vector<listEdge*>& getEdges () {std::vector<listEdge*> tab; tab = this->edges; return (this->edges);}
};

class listEdge
{
    int value = 0;
    listVertice* startVert = nullptr;
    listVertice* endVert = nullptr;
    listEdge* onList = nullptr;
 public:
    std::vector<listEdge*>* startVrtEdgs;
    std::vector<listEdge*>* endVrtEdgs;

 public:
     listEdge () {}
     listEdge (listVertice* stVt, listVertice* edVt) {*(this->startVert) = *(stVt); *(this->startVrtEdgs) = stVt->getEdges();
                                                      *(this->endVert) = *(edVt); *(this->endVrtEdgs) = edVt->getEdges();}
     listEdge (const listEdge& edg) {this-> value = edg.value;
                                     this->startVert = new listVertice; this->startVert = edg.startVert;
                                     this->endVert = new listVertice; this->endVert = edg.endVert;
                                     this->onList = new listEdge; this->onList = edg.onList;}
     ~listEdge () {delete this->onList; delete this->startVrtEdgs; delete this->endVrtEdgs;}
//     listEdge& operator= (const listEdge edg) {delete this->startVert; delete this->endVert; delete this->onList; delete this->startVrtEdgs; delete this->endVrtEdgs;
//                                       this->value = edg.retValue();
//                                       this->startVert = edg.retStart();
//                                       this->endVert = edg.retEnd();
//                                       this->onList = edg.retOnList();
//                                       this->startVrtEdgs = edg.startVrtEdgs;
//                                       this->endVrtEdgs = edg.endVrtEdgs; return *this;}
     listEdge& operator= (const int val) {this->value = val; return *this;}
     void setStartEnd (listVertice* start, listVertice* end) {this->startVert = start; this->endVert = end;}
     void writeOnList (listEdge* ptr) {*(this->onList) = *(ptr);}
     int retValue () const {return this->value;}
     listVertice* retStart () const {return this->startVert;}
     listVertice* retEnd () const {return this->endVert;}
     listEdge*& retOnList () {return this->onList;}
};

//void listVertice::addEdge (listEdge* newEd) {this->edges.push_back(newEd);}

/****************************** Klasy dla macierzy sasiedztwa. ******************************/

class mtxVertice
{
    vertice vert;
    int index;

 public:
     mtxVertice (): vert(), index(0) {}
     mtxVertice (int ind): vert(), index(ind) {}
     mtxVertice (const mtxVertice& vrt) {this->vert = vrt.vert; this->index = vrt.index;}
     ~mtxVertice () {}
     mtxVertice& operator= (mtxVertice vrt) {this->vert = *(vrt.retVert() ); this->index = vrt.retIndex(); return *this;}
     bool operator== (mtxVertice vrt) {if(this->index == vrt.index) return true; else return false;}
     vertice* retVert () {return &(this->vert);}
     int retIndex () const {return this->index;}
};

class mtxEdge
{
    int value = 0;
    mtxVertice* startVert = nullptr;
    mtxVertice* endVert = nullptr;
    mtxEdge* onList = nullptr;

 public:
     mtxEdge () {}
     mtxEdge (mtxVertice* stVt, mtxVertice* edVt) {*(this->startVert) = *(stVt); *(this->endVert) = *(edVt);}
     mtxEdge (const mtxEdge& edg) {this-> value = edg.value;
                              this->startVert = new mtxVertice; *(this->startVert) = *(edg.startVert);
                              this->endVert = new mtxVertice; *(this->endVert) = *(edg.endVert);
                              this->onList = new mtxEdge; this->onList = edg.onList;}
     ~mtxEdge () {delete this->startVert; delete this->endVert; delete this->onList;}
     mtxEdge& operator= (const mtxEdge edg) {this->value = edg.retValue();
                                             this->startVert = edg.retStart();
                                             this->endVert = edg.retEnd();
                                             this->onList = edg.retOnList(); return *this;}
     mtxEdge& operator= (const int val) {this->value = val; return *this;}
     void setStartEnd (mtxVertice* start, mtxVertice* end) {this->startVert = start; this->endVert = end;}
     void writeOnList (mtxEdge* ptr) {*(this->onList) = *(ptr);}
     int retValue () const {return this->value;}
     mtxVertice* retStart () const {return this->startVert;}
     mtxVertice* retEnd () const {return this->endVert;}
     mtxEdge* retOnList () const {return this->onList;}
};

#endif //EDGE_LIST