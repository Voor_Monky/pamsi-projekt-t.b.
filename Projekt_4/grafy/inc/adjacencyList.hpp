#ifndef ADJACENCY_LIST
#define ADJACENCY_LIST

#include "vertice&edge.hpp"

class adjList
{
    std::vector<listVertice> verticeList;
    std::vector<listEdge> edgeList;

 public:
    adjList (): verticeList(), edgeList() {}
    adjList (const adjList& graph) {this->verticeList = graph.verticeList; this->edgeList = graph.edgeList;}
    adjList (adjList&& graph): verticeList(std::move(graph.verticeList) ), edgeList(std::move(graph.edgeList) ) {} //= delete;
    ~adjList () {}
    std::vector<listVertice*> endVertices (listEdge* /*edge*/);
    listVertice* opposite (listVertice* /*vert*/, listEdge* /*edge*/);
    bool areAdjacent (listVertice* /*vert1*/, listVertice* /*vert2*/);
    void replace (listVertice* /*vert*/, int /*elem*/);
    void replace (listEdge* /*edge*/, int /*value*/);
    void insertVertice (int /*elem*/);
    void insertEdge (listVertice* /*start*/, listVertice* /*end*/, int /*elem*/);
    void removeVertice (listVertice* /*vert*/);
    void removeEdge (listEdge* /*edge*/);
    std::vector<listEdge*> incidentEdges (listVertice* /*vert*/);
    std::vector<listVertice>* retVertices ();
    std::vector<listEdge>* retEdges ();
    adjList& operator= (const adjList graph) {this->verticeList = graph.verticeList; this->edgeList = graph.edgeList; return *this;}
};

std::istream& operator>> (std::istream& /*strm*/, adjList& /*graph*/);
std::ostream& operator<< (std::ostream& /*strm*/, adjList& /*graph*/);

#endif //ADJACENCY_LIST