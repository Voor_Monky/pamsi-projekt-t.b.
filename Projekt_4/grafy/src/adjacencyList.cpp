#include <iostream>
#include "../inc/adjacencyList.hpp"

std::vector<listVertice*> adjList::endVertices (listEdge* edge)
{
    std::vector<listVertice*> vTab;
    vTab.push_back(edge->retStart() );
    vTab.push_back(edge->retEnd() );

    return vTab;
}

listVertice* adjList::opposite (listVertice* vert, listEdge* edge)
{
    if(*(edge->retStart() ) == *vert)
        {return edge->retEnd();}
    if(*(edge->retEnd() ) == *vert)
        {return edge->retStart();}
    else
        {return nullptr;}
}

bool adjList::areAdjacent (listVertice* vert1, listVertice* vert2)
{
    bool areThey = false;
    std::vector<listEdge*> incEdg1 = vert1->getEdges(), incEdg2 = vert2->getEdges();

    for(unsigned int i = 0; i < incEdg1.size(); ++i)
    {
        for(unsigned int j = 0; j < incEdg2.size(); ++j)
        {
            if(incEdg1.at(i) == incEdg2.at(j) )
                {
                    areThey = true;
                    break;
                }
        }
    }

    return areThey;
}

void adjList::replace (listVertice* vert, int elem)
    {*(vert) = elem;}

void adjList::replace (listEdge* edge, int value)
    {*(edge) = value;}

void adjList::insertVertice (int elem)
{
//    listVertice* helper;

    listVertice* vert = new listVertice(elem);

    int tmp = this->verticeList.size();
    vert->retIndex() = tmp;

    this->verticeList.push_back(*vert);

//    helper = vert->retOnList();
//    *helper = *(--this->verticeList.end() );
}

void adjList::insertEdge (listVertice* start, listVertice* end, int elem)
{
    listEdge* edge = new listEdge;


    *(edge) = elem;
    edge->setStartEnd(start, end);

    edge->startVrtEdgs = &(start->getEdges() );
    edge->startVrtEdgs->push_back(edge);

    edge->endVrtEdgs = &(end->getEdges() );
    edge->endVrtEdgs->push_back(edge);

    this->edgeList.push_back(*(edge) );

//    edge->retOnList() = *(--this->edgeList.end() );
}

void adjList::removeVertice (listVertice* vert)
{
    std::vector<listEdge*> helper = vert->getEdges();
    for(unsigned int i = 0; i < helper.size(); ++i)
        {removeEdge(helper.at(i) );}
    delete vert;
}

void adjList::removeEdge (listEdge* edge)
    {delete edge;}

std::vector<listEdge*> adjList::incidentEdges (listVertice* vert)
    {return vert->getEdges();}

std::vector<listVertice>* adjList::retVertices ()
    {return &(this->verticeList);}

std::vector<listEdge>* adjList::retEdges ()
    {return &(this->edgeList);}

/*Operatory wczytywania i wypisywania grafu*/
std::istream& operator>> (std::istream& strm, adjList& graph)
{
    int edgeNum = 0, startVertice = 0, endVertice = 0, value = 0;
    std::vector<listVertice>* verts = graph.retVertices();
    graph = *(new adjList);

    if(!(strm >> value) )   //number of vertices
        {return strm;}

    for(int i = 0; i < value; ++i)
        {graph.insertVertice(i);}

    if(!(strm >> edgeNum) )
        {return strm;}

    for(int i = 0; i < edgeNum; ++i)
    {
        if(!(strm >> startVertice) )
            {return strm;}
        if(!(strm >> endVertice) )
            {return strm;}
        if(!(strm >> value) )
            {return strm;}

        graph.insertEdge(&(verts->at(startVertice) ), &(verts->at(endVertice) ), value);
    }

    return strm;
}

std::ostream& operator<< (std::ostream& strm, adjList& graph)
{
    std::vector<listEdge>* edges = graph.retEdges();

    strm << graph.retVertices()->size()
         << '\t'
         << edges->size()
         << std::endl;

    for(unsigned int i = 0; i < edges->size(); ++i)
    {
        strm << edges->at(i).retStart()->retIndex()
             << '\t'
             << edges->at(i).retEnd()->retIndex()
             << '\t'
             << edges->at(i).retValue()
             << std::endl;
    }

    return strm;
}