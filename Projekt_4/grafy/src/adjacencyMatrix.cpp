#include <iostream>
#include "../inc/adjacencyMatrix.hpp"

adjMatrix::adjMatrix ()
{
    for(unsigned int i = 0; i < this->adjMtx.size(); ++i)
    {
        std::fill(this->adjMtx.at(i).begin(), this->adjMtx.at(i).end(), nullptr);
    }
}

std::vector<mtxVertice*> adjMatrix::endVertices (mtxEdge* edge)
{
    std::vector<mtxVertice*> vTab;
    vTab.push_back(edge->retStart() );
    vTab.push_back(edge->retEnd() );

    return vTab;
}

mtxVertice* adjMatrix::opposite (mtxVertice* vert, mtxEdge* edge)
{
    if(*(edge->retStart() ) == *vert)
        {return edge->retEnd();}
    if(*(edge->retEnd() ) == *vert)
        {return edge->retStart();}
    else
        {return nullptr;}
}

bool adjMatrix::areAdjacent (mtxVertice* vert1, mtxVertice* vert2)
{
    int ind1 = vert1->retIndex(), ind2 = vert2->retIndex();

    if(this->adjMtx.at(ind1).at(ind2) == nullptr)
        {return false;}
    else
        {return true;}
}

void adjMatrix::replace (mtxVertice* vert, int elem)
    {*(vert->retVert() ) = elem;}

void adjMatrix::replace (mtxEdge* edge, int value)
    {*(edge) = value;}

void adjMatrix::insertVertice (int elem)
{
    int ind = this->adjMtx.size();
    mtxVertice* vert = new mtxVertice(ind);
    *(vert->retVert() ) = elem;

    this->verticeList.push_back(*vert);

    std::vector<mtxEdge*> helper;
    helper.push_back(nullptr);
    for(unsigned int i = 0; i < this->adjMtx.size(); ++i)
    {
        this->adjMtx.at(i).push_back(nullptr);
        helper.push_back(nullptr);
    }
    this->adjMtx.push_back(helper);
}

void adjMatrix::insertEdge (mtxVertice* start, mtxVertice* end, int elem)
{
    mtxEdge* edge = new mtxEdge;
    *(edge) = elem;
    edge->setStartEnd(start, end);

    this->edgeList.push_back(*edge);

    this->adjMtx.at(start->retIndex()).at(end->retIndex()) = edge;
    this->adjMtx.at(end->retIndex()).at(start->retIndex()) = edge;
}

void adjMatrix::removeVertice (mtxVertice* vert)
{
    int ind = vert->retIndex();
    for(unsigned int i = 0; i < this->adjMtx.size(); ++i)
    {
        removeEdge(this->adjMtx.at(ind).at(i) );
        removeEdge(this->adjMtx.at(i).at(ind) );
    }

    delete vert;
}

void adjMatrix::removeEdge (mtxEdge* edge)
{
    if(edge == nullptr)
        {return;}

    int ind1 = edge->retStart()->retIndex(), ind2 = edge->retEnd()->retIndex();

    this->adjMtx.at(ind1).at(ind2) = nullptr;
    this->adjMtx.at(ind2).at(ind1) = nullptr;

    delete edge;
}

std::vector<mtxEdge*> adjMatrix::incidentEdges (mtxVertice* vert)
{
    std::vector<mtxEdge*> result;
    int ind = vert->retIndex();

    for(unsigned int i = 0; i < this->adjMtx.size(); ++i)
    {
        if(this->adjMtx.at(ind).at(i) != nullptr)
        {
            result.push_back(this->adjMtx.at(ind).at(i) );
        }
    }

    return result;
}

std::vector<mtxVertice>* adjMatrix::retVertices ()
    {return &(this->verticeList);}

std::vector<mtxEdge>* adjMatrix::retEdges ()
    {return &(this->edgeList);}

/*Operatory wczytywania i wypisywania grafu*/
std::istream& operator>> (std::istream& strm, adjMatrix& graph)
{
    int edgeNum = 0, startVertice = 0, endVertice = 0, value = 0;
    std::vector<mtxVertice>* verts;
    graph = *(new adjMatrix);

    if(!(strm >> value) )   //number of vertices
        {return strm;}
    if(value == 0)
        {return strm;}
    for(int i = 0; i < value; ++i)
        {graph.insertVertice(i);}
    if(!(strm >> edgeNum) )     //number of edges
        {return strm;}
    if(edgeNum == 0)
        {return strm;}

    verts = graph.retVertices();

    for(int i = 0; i < edgeNum; ++i)
    {
        if(!(strm >> startVertice) )
            {return strm;}
        if(!(strm >> endVertice) )
            {return strm;}
        if(!(strm >> value) )
            {return strm;}

        graph.insertEdge(&(verts->at(startVertice) ), &(verts->at(endVertice) ), value);
    }

    return strm;
}

std::ostream& operator<< (std::ostream& strm, adjMatrix& graph)
{
    std::vector<mtxEdge>* edges = graph.retEdges();

    strm << graph.retVertices()->size()
         << '\t'
         << edges->size()
         << std::endl;

    for(unsigned int i = 0; i < edges->size(); ++i)
    {
        strm << edges->at(i).retStart()->retIndex()
             << '\t'
             << edges->at(i).retEnd()->retIndex()
             << '\t'
             << edges->at(i).retValue()
             << std::endl;
    }

    return strm;
}