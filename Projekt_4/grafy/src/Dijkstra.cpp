#ifndef DIJKSTRA
#define DIJKSTRA

#include <queue>
#include <vector>
#include <iostream>

#define MAXINT 2147483647

template<typename graphType>
std::vector<std::vector<int> > dijkstra (graphType graph)
{
    int helper = 0, tmp = 0;

    std::vector<bool> isChecked;
    isChecked.resize(graph.retVertices()->size() );
    std::fill(isChecked.begin(), isChecked.end(), false);

    std::vector<int> costs;
    costs.resize(graph.retVertices()->size() );
    std::fill(costs.begin(), costs.end(), MAXINT);
    costs.at(0) = 0;

    std::vector<int> predecessors;
    predecessors.resize(graph.retVertices()->size() );
    std::fill(predecessors.begin(), predecessors.end(), -1);

    std::priority_queue<int, std::vector<int>, std::greater<int> > costQueue;

    for(unsigned int i = 0; i < graph.retVertices()->size(); ++i)
    {
        while(!costQueue.empty())
            {costQueue.pop();}
        for(unsigned int l = 0; l < costs.size(); ++l)
            {
                if(!(isChecked.at(l) ) )
                    {costQueue.push(costs.at(l) );}
            }

        helper = costQueue.top();
        for(unsigned int j = 0; j < costs.size(); ++j)
        {
            if(costs.at(j) == helper)
            {
                helper = j;
                break;
            }
        }

        isChecked.at(helper) = true;

        for(unsigned int j = 0; j < graph.incidentEdges(&(graph.retVertices()->at(helper) ) ).size(); ++j)
        {
            if(!(graph.opposite(&(graph.retVertices()->at(helper) ), graph.incidentEdges(&(graph.retVertices()->at(helper) ) ).at(j) ) == nullptr) )
            {
                tmp = graph.opposite(&(graph.retVertices()->at(helper) ), graph.incidentEdges(&(graph.retVertices()->at(helper) ) ).at(j) )->retIndex();

                if(!(isChecked.at(tmp) == true) )  //if end
                {
                    if(costs.at(tmp) > (costs.at(helper) + graph.incidentEdges(&(graph.retVertices()->at(helper) ) ).at(j)->retValue() ) )
                    {
                        costs.at(tmp) = (costs.at(helper) + graph.incidentEdges(&(graph.retVertices()->at(helper) ) ).at(j)->retValue() );
                        predecessors.at(tmp) = helper;
                    }
                }
            }
        }
    }

    std::vector<std::vector<int> > result;    //start vertice, path, cost, path, cost, ...
    result.resize(graph.retVertices()->size() * 2);
    result.at(0).push_back(graph.retVertices()->at(0).retIndex() );

    unsigned int t = 1;
    for(unsigned int i = 1; i < costs.size(); ++i)
    {
        result.at(t).push_back(i);

        for(int j = predecessors.at(i); j > -1;)
        {
            result.at(t).push_back(j);
            j = predecessors.at(j);
        }

        result.at(t + 1).push_back(costs.at(i) );
        t = t + 2;
    }

    return result;
}

#endif //DIJKSTRA