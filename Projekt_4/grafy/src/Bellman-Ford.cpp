#ifndef BELLFORD
#define BELLFORD

#include <vector>
#include <iostream>

#define MAXINT 2147483647

template<typename graphType>
std::vector<std::vector<int> > bellford (graphType graph)
{
    bool test = false;
    int tmp = 0;

    std::vector<unsigned int> costs;
    costs.resize(graph.retVertices()->size() );
    std::fill(costs.begin(), costs.end(), MAXINT);
    costs.at(0) = 0;

    std::vector<int> predecessors;
    predecessors.resize(graph.retVertices()->size() );
    std::fill(predecessors.begin(), predecessors.end(), -1);

    for(unsigned int i = 1; i < costs.size(); ++i)
    {
        test = true;

        for(unsigned int k = 0; k < costs.size() - 1; ++k)
        {
            for(unsigned int j = 0; j < graph.incidentEdges(&(graph.retVertices()->at(k) ) ).size(); ++j)
            {
                if(!(graph.opposite(&(graph.retVertices()->at(k) ), graph.incidentEdges(&(graph.retVertices()->at(k) ) ).at(j) ) == nullptr) )
                {
                    tmp = graph.opposite(&(graph.retVertices()->at(k) ), graph.incidentEdges(&(graph.retVertices()->at(k) ) ).at(j) )->retIndex();

                    if(tmp != 0)
                    {
                        if(costs.at(tmp) > costs.at(k) + graph.incidentEdges(&(graph.retVertices()->at(k) ) ).at(j)->retValue() )
                        {
                            test = false;
                            costs.at(tmp) = costs.at(k) + graph.incidentEdges(&(graph.retVertices()->at(k) ) ).at(j)->retValue();
                            predecessors.at(tmp) = k;
                        }
                    }
                }
            }
        }
        if(test == true)
            {break;}
    }

    std::vector<std::vector<int> > result;    //start vertice, path, cost, path, cost, ...
    result.resize(graph.retVertices()->size() * 2);
    result.at(0).push_back(graph.retVertices()->at(0).retIndex() );

    unsigned int t = 1;
    for(unsigned int i = 1; i < costs.size(); ++i)
    {
        result.at(t).push_back(i);

        for(int j = predecessors.at(i); j > -1;)
        {
            result.at(t).push_back(j);
            j = predecessors.at(j);
        }

        result.at(t + 1).push_back(costs.at(i) );
        t = t + 2;
    }

    return result;
}

#endif  //BELLFORD