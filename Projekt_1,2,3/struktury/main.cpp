#include "inc/lista.hpp"
#include "inc/stos.hpp"
#include "inc/kolejka.hpp"
#include "inc/kolejprior.hpp"
#include "inc/hashtab.hpp"

#include <iostream>
#include <vector>
#include <stack>
#include <queue>

int main()
{
    int elem1 =1, elem2 = 10, elem3 = 11, elem4 =5;
    //Przyklady dla listy
    lista<int> listao;
    listao.dodajPoczatek(elem1); listao.dodajKoniec(elem2); listao.dodajKoniec(elem3); listao.dodaj(elem4, 2);
    lista<int>::listaIterator itero;
    itero.wskazLista(listao);
    std::cout << "Cala lista:" << std:: endl;
    for(int i = 0; i < 4; ++i, ++itero)
    {
        std::cout << itero.iter->zwrocWartosc() << '\t';
    }
    std::cout << std::endl;
    listao.usun(1);
    std::cout << "Element 2., po usunieciu go:" << std:: endl
              << listao[1].zwrocWartosc() << std::endl;
    //vector
    std::vector<int> vect;
    vect.push_back(elem1); vect.push_back(elem2); vect.push_back(elem3); vect.insert(++++vect.begin(), elem4);
    std::cout << "Vector:" << std::endl << "Cala lista:" << std:: endl;
    for(auto i = 0; i < vect.size(); ++i)
    {
        std::cout << vect.at(i) << '\t';
    }
    std::cout << std::endl;
    vect.erase(++vect.begin());
    std::cout << "Element 2., po usunieciu go:" << std:: endl;
    std::cout << vect.at(1) << std::endl;

    //Przyklady dla stosu
    stos<int> stosik;
    stosik.push(elem1); stosik.push(elem2); stosik.push(elem3); stosik.push(elem4);
    std::cout << "Caly stos:" << std:: endl;
    for(unsigned int i = 0; i < stosik.zwrocRozmiar(); ++i)
    {
        elemListy<int>* tmp = stosik.zwrocWierzch();
        for(unsigned int j = 0; j < i; ++j)
        {
            tmp = tmp->coPrzed();
        }
        std::cout << tmp->zwrocWartosc() << '\t';
    }
    std::cout << std::endl;
    std::cout << "Dzialanie pop:" << std::endl
              << stosik.pop() << '\t' << stosik.pop() << std::endl;
    //stack
    std::stack<int> stacc;
    stacc.push(elem1); stacc.push(elem2); stacc.push(elem3); stacc.push(elem4);
    std::cout << "Stack:" << std::endl
              << "Caly stos:" << std::endl;
    for(int i = 0; i < stacc.size();)
    {
        std::cout << stacc.top() << '\t';
        stacc.pop();
    }
    std::cout << std::endl;

    //Przyklady dla kolejki
    kolejka<int> kolej;
    kolej.enqueue(elem1); kolej.enqueue(elem2); kolej.enqueue(elem3); kolej.enqueue(elem4);
    std::cout << "Cala kolejka:" <<std::endl;
    for(int i = 0; i < kolej.zwrocRozmiar();)
    {
        std::cout << kolej.dequeue() << '\t';
    }
    std::cout << std::endl;
    //Queue
    std::queue<int> kueue;
    kueue.push(elem1); kueue.push(elem2); kueue.push(elem3); kueue.push(elem4);
    std::cout << "Queue:" << std::endl
              << "Cala kolejka:" << std::endl;
    for(int i = 0; i < kueue.size();)
    {
        std::cout << kueue.front() << '\t';
        kueue.pop();
    }
    std::cout << std::endl;

    //Przyklady kolejki priorytetowej
    kolejprior<int> kopri;
//    kopri.enqueue(elem1, elem4); kopri.enqueue(elem2, elem3); kopri.enqueue(elem3, elem2); kopri.enqueue(elem4, elem1);
//    std::cout << "Cala kolejka priorytetowa:" <<std::endl;
//    for(int i = 0; i < kopri.zwrocRozmiar();)
//    {
//        std::cout << kopri.dequeue() << '\t';
//    }
//    std::cout << std::endl;

    return 0;
}
