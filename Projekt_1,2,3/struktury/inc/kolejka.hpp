#ifndef KOLEJKA
#define KOLEJKA

#include "lista.hpp"

/*! Kolejka */
template<typename typ>
class kolejka
{
    unsigned int rozmiar;
    elemListy<typ>* poczatek;
    elemListy<typ>* koniec;

 public:
     kolejka();
     ~kolejka();
     unsigned int zwrocRozmiar();
     bool enqueue(typ&);
     typ dequeue();
};

template<typename typ>
kolejka<typ>::kolejka()
{
    elemListy<typ>* elem = new elemListy<typ>(0, nullptr, nullptr);
    this->rozmiar = 0;
    this->poczatek = nullptr;
    this->koniec = elem;
}

template<typename typ>
kolejka<typ>::~kolejka()
{
    if(poczatek != nullptr)
    {
        elemListy<typ>* wsk = poczatek;
        elemListy<typ>* tmp = poczatek->coZa();

        while(wsk != nullptr)
        {
            delete wsk;
            wsk = tmp;
            if(wsk != nullptr)
            {
                tmp = wsk->coZa();
            }
        }
    }
}

template<typename typ>
unsigned int kolejka<typ>::zwrocRozmiar()
{
    return this->rozmiar;
}

template<typename typ>
bool kolejka<typ>::enqueue(typ& nowyElem)
{
    if(elemListy<typ>* elem = new elemListy<typ>(nowyElem, this->koniec->coPrzed(), this->koniec))
    {
        if(this->poczatek == nullptr)
        {
            this->poczatek = elem;
        }
        if(this->koniec->coPrzed() !=nullptr)
        {
            this->koniec->coPrzed()->zmienZa(elem);
        }
        this->koniec->zmienPrzed(elem);
        this->rozmiar = this->rozmiar + 1;

        return true;
    }
    return false;
}

template<typename typ>
typ kolejka<typ>::dequeue()
{
    typ tmp = this->poczatek->zwrocWartosc();
    this->poczatek = poczatek->coZa();
    delete this->poczatek->coPrzed();
    this->rozmiar = this->rozmiar - 1;

    return tmp;
}

#endif // KOLEJKA
