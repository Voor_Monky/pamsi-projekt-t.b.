#ifndef KOLEJPRIOR
#define KOLEJPRIOR

#include "lista.hpp"

/*! Element kolejki priorytetowej */
template<typename typ>
class elemKolejprior
{
    unsigned int priorytet;

 public:
     elemListy<typ> element;

     elemKolejprior();
     elemKolejprior(typ, elemListy<typ>*, elemListy<typ>*, unsigned int);
     ~elemKolejprior(){}
     bool operator>(elemKolejprior);
     bool operator<(elemKolejprior);
     elemKolejprior<typ>* coZa();
};

template<typename typ>
elemKolejprior<typ>::elemKolejprior()
{
    this->element = *(new elemListy<typ>(0, nullptr, nullptr));
    this->priorytet = 0;
}

template<typename typ>
elemKolejprior<typ>::elemKolejprior(typ zawartosc, elemListy<typ>* poprzed, elemListy<typ>* nast, unsigned int prior)
{
    this->element = *(new elemListy<typ>(zawartosc, poprzed, nast));
    this->priorytet = prior;
}

template<typename typ>
bool elemKolejprior<typ>::operator>(elemKolejprior obiekt)
{
    if(this->priorytet > obiekt.priorytet)
        return true;

    return false;
}

template<typename typ>
bool elemKolejprior<typ>::operator<(elemKolejprior obiekt)
{
    if(this->priorytet < obiekt.priorytet)
        return true;

    return false;
}

template<typename typ>
elemKolejprior<typ>* elemKolejprior<typ>::coZa()
{
    elemKolejprior<typ>* wynik = new elemKolejprior<typ>(this->element.zwrocWartosc(), this->element.coPrzed(), this->element.coZa(), this->priorytet);

    return wynik;
}

/*! Kolejka priorytetowa */
template<typename typ>
class kolejprior
{
    unsigned int rozmiar;
    elemKolejprior<typ>* poczatek;

 public:
     kolejprior();
     ~kolejprior();
     unsigned int zwrocRozmiar();
     void enqueue(typ&, unsigned int);
     typ dequeue();
};

template<typename typ>
kolejprior<typ>::kolejprior()
{
    this->rozmiar = 0;
    this->poczatek = nullptr;
}

template<typename typ>
kolejprior<typ>::~kolejprior()
{
    if(poczatek != nullptr)
    {
        elemKolejprior<typ>* wsk = poczatek;
        elemKolejprior<typ>* tmp = poczatek->coZa();

        while(wsk != nullptr)
        {
            delete wsk;
            wsk = tmp;
            if(wsk != nullptr)
            {
                tmp = wsk->coZa();
            }
        }
    }
}

template<typename typ>
unsigned int kolejprior<typ>::zwrocRozmiar()
{
    return this->rozmiar;
}

template<typename typ>
void kolejprior<typ>::enqueue(typ& nowyElem, unsigned int priorytet)
{
    elemKolejprior<typ>* elem = new elemKolejprior<typ>(nowyElem, nullptr, nullptr, priorytet);

    for(unsigned int i = 0; i < this->rozmiar; ++i)
    {
        elemKolejprior<typ>* tmp = this->poczatek;
        for(unsigned int j = 0; j < i; ++j)
        {
            tmp = tmp->coZa();
        }
        if(elem > tmp)
        {
            elem->element.zmienZa(&(tmp->element));
            if(tmp != this->poczatek)
            {
                tmp = this->poczatek;
                for(unsigned int j = 0; j < i-1; ++j)
                {
                    tmp = tmp->coZa();
                }
                tmp->element.zmienZa(&(elem->element));
                break;
            }
            else
            {
                this->poczatek = elem;
                break;
            }
        }
        if(i+1 >= this->rozmiar)
        {
            tmp->element.zmienZa(&(elem->element));
            break;
        }
    }

    this->rozmiar = this->rozmiar + 1;
}

template<typename typ>
typ kolejprior<typ>::dequeue()
{
    elemKolejprior<typ>* helper = this->poczatek;
    typ tmp = helper->element.zwrocWartosc();
    this->poczatek = this->poczatek->coZa();
    this->rozmiar = this->rozmiar - 1;
    delete helper;

    return tmp;
}

#endif // KOLEJPRIOR
