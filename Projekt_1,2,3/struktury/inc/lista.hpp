#ifndef LISTA
#define LISTA

/*! Element listy. */
template<typename typ>
class elemListy
{
    typ wartosc;
    elemListy* poprzedni;
    elemListy* nastepny;

 public:
     elemListy();
     elemListy(typ, elemListy*, elemListy*);
     ~elemListy(){}
     typ zwrocWartosc();
     typ zwrocWartosc() const;
     bool zmienPrzed(elemListy*);
     bool zmienZa(elemListy*);
     elemListy* coPrzed() const;
     elemListy* coZa() const;
     elemListy operator= (elemListy& elem);
};

template<typename typ>
elemListy<typ>::elemListy()
{
    this->wartosc = 0;
    this->poprzedni = nullptr;
    this->nastepny = nullptr;
}

template<typename typ>
elemListy<typ>::elemListy(typ zawartosc, elemListy* poprzed, elemListy* nast)
{
    this->wartosc = zawartosc;
    this->poprzedni = poprzed;
    this->nastepny = nast;
}

template<typename typ>
typ elemListy<typ>::zwrocWartosc()
{
    return this->wartosc;
}

template<typename typ>
typ elemListy<typ>::zwrocWartosc() const
{
    return this->wartosc;
}

template<typename typ>
bool elemListy<typ>::zmienPrzed(elemListy* wskaznik)
{
    if(this->poprzedni = wskaznik)
        return true;

    return false;
}

template<typename typ>
bool elemListy<typ>::zmienZa(elemListy* wskaznik)
{
    if(this->nastepny = wskaznik)
        return true;

    return false;
}

template<typename typ>
elemListy<typ>* elemListy<typ>::coPrzed() const
{
    return this->poprzedni;
}

template<typename typ>
elemListy<typ>* elemListy<typ>::coZa() const
{
    return this->nastepny;
}

template<typename typ>
elemListy<typ> elemListy<typ>::operator= (elemListy& elem)
{
    this->wartosc = elem.zwrocWartosc();
    this->poprzedni = elem.coPrzed();
    this->nastepny = elem.coZa();

    return *this;
}

/*! Lista. */
template<typename typ>
class lista
{
    elemListy<typ>* glowa;
    elemListy<typ>* ogon;

 public:
     lista();
     ~lista();
     class listaIterator;
     elemListy<typ>* dajGlowe();
     elemListy<typ> operator[](int);
     elemListy<typ> operator[](int) const;
     bool dodajPoczatek(typ&);
     bool dodajKoniec(typ&);
     bool dodaj(typ&, int);
     bool usun(int);
};

/*! Iterator listy. */
template<typename typ>
class lista<typ>::listaIterator
{
 public:
     elemListy<typ>* iter;

     listaIterator();
     bool wskazLista(lista&);
     lista<typ>::listaIterator& operator++();
     lista<typ>::listaIterator& operator--();
     typ operator*();
};

template<typename typ>
lista<typ>::listaIterator::listaIterator()
{
    this->iter = nullptr;
}

template<typename typ>
bool lista<typ>::listaIterator::wskazLista(lista& tmpList)
{
    if(this->iter = tmpList.dajGlowe())
        return true;

    return false;
}

template<typename typ>
typename lista<typ>::listaIterator& lista<typ>::listaIterator::operator++()
{
    this->iter = iter->coZa();

    return *this;
}

template<typename typ>
typename lista<typ>::listaIterator& lista<typ>::listaIterator::operator--()
{
    this->iter = iter->coPrzed();

    return *this;
}

template<typename typ>
typ lista<typ>::listaIterator::operator*()
{
    return this->iter->zwrocWartosc();
}

/*! Metody klasy lista. */
template<typename typ>
lista<typ>::lista()
{
    elemListy<typ>* elem = new elemListy<typ>(0, nullptr, nullptr);
    this->glowa = elem;
    this->ogon = elem;
}

template<typename typ>
lista<typ>::~lista()
{
    if(glowa != nullptr)
    {
        elemListy<typ>* wsk = glowa;
        elemListy<typ>* tmp = glowa->coZa();

        while(wsk != nullptr)
        {
            delete wsk;
            wsk = tmp;
            if(wsk != nullptr)
            {
                tmp = wsk->coZa();
            }
        }
    }
}

template<typename typ>
elemListy<typ>* lista<typ>::dajGlowe()
{
    return this->glowa;
}

template<typename typ>
elemListy<typ> lista<typ>::operator[](int indeks)
{
    lista<typ>::listaIterator iterator; iterator.wskazLista(*this);
    for(int i = 0; i < indeks; ++i)
    {
        ++iterator;
    }

    return *iterator.iter;
}

template<typename typ>
elemListy<typ> lista<typ>::operator[](int indeks) const
{
    lista<typ>::listaIterator iterator; iterator.wskazLista(*this);
    for(int i = 0; i < indeks; ++i)
    {
        ++iterator;
    }

    return *iterator.iter;
}

template<typename typ>
bool lista<typ>::dodajPoczatek(typ& zawartosc)
{
    if(elemListy<typ>* elem = new elemListy<typ>(zawartosc, nullptr, this->glowa))
    {
        this->glowa->zmienPrzed(elem);
        this->glowa = elem;
        return true;
    }

    return false;
}

template<typename typ>
bool lista<typ>::dodajKoniec(typ& zawartosc)
{
    if(elemListy<typ>* elem = new elemListy<typ>(zawartosc, this->ogon->coPrzed(), ogon))
    {
        this->ogon->coPrzed()->zmienZa(elem);
        this->ogon->zmienPrzed(elem);
        return true;
    }

    return false;
}

template<typename typ>
bool lista<typ>::dodaj(typ& zawartosc, int indeks)
{
    lista<typ>::listaIterator iterator; iterator.wskazLista(*this);
    for(int i = 0; i < indeks; ++i)
    {
        ++iterator;
    }

    if(elemListy<typ>* elem = new elemListy<typ>(zawartosc, iterator.iter->coPrzed() ,iterator.iter))
    {
        iterator.iter->coPrzed()->zmienZa(elem);
        iterator.iter->zmienPrzed(elem);
        return true;
    }

    return false;
}

template<typename typ>
bool lista<typ>::usun(int indeks)
{
    lista<typ>::listaIterator iterator; iterator.wskazLista(*this);
    for(int i = 0; i < indeks; ++i)
    {
        ++iterator;
    }

    iterator.iter->coPrzed()->zmienZa(iterator.iter->coZa());
    iterator.iter->coZa()->zmienPrzed(iterator.iter->coPrzed());
    delete iterator.iter;

    return true;
}

#endif //STOS
