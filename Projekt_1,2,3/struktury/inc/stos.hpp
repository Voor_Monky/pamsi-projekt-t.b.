#ifndef STOS
#define STOS

#include "lista.hpp"

/*! Stos */
template<typename typ>
class stos
{
    unsigned int rozmiar;
    elemListy<typ>* wierzch;

 public:
     stos();
     ~stos();
     unsigned int zwrocRozmiar();
     elemListy<typ>* zwrocWierzch();
     bool push(typ&);
     typ pop();
};

template<typename typ>
stos<typ>::stos()
{
    elemListy<typ>* elem = new elemListy<typ>(0, nullptr, nullptr);
    this->rozmiar = 0;
    this->wierzch = elem;
}

template<typename typ>
stos<typ>::~stos()
{
    if(wierzch != nullptr)
    {
        elemListy<typ>* wsk = wierzch;
        elemListy<typ>* tmp = wierzch->coPrzed();

        while(wsk != nullptr)
        {
            delete wsk;
            wsk = tmp;
            if(wsk != nullptr)
            {
                tmp = wsk->coPrzed();
            }
        }
    }
}

template<typename typ>
unsigned int stos<typ>::zwrocRozmiar()
{
    return this->rozmiar;
}

template<typename typ>
elemListy<typ>* stos<typ>::zwrocWierzch()
{
    return this->wierzch;
}

template<typename typ>
bool stos<typ>::push(typ& nowyElem)
{
    if(elemListy<typ>* elem = new elemListy<typ>(nowyElem, this->wierzch, nullptr))
    {
        this->wierzch->zmienZa(elem);
        this->wierzch = elem;
        this->rozmiar = this->rozmiar + 1;

        return true;
    }

    return false;
}

template<typename typ>
typ stos<typ>::pop()
{
    typ tmp = this->wierzch->zwrocWartosc();
    this->wierzch = wierzch->coPrzed();
    delete this->wierzch->coZa();

    return tmp;
}

#endif //STOS
