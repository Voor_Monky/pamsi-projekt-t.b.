#include "include/pamsi_tester/Tester.hpp"
#include "include/spdlog/spdlog.h"
#include <fstream>
#include <numeric>
#include <iostream>
#include <string>
#include<vector>

class macierz
{
    public:
    unsigned int lWierszy = 0;
    unsigned int lKolumn = 0;
    std::vector<float> elementy;

    ~macierz() {}
    float& operator [] (unsigned int elem_num) {return elementy[elem_num];}
    const float& operator [] (unsigned int elem_num) const {return elementy[elem_num];}
    macierz operator + (macierz mtx) const
    {
        macierz wynik; wynik.lWierszy = this->lWierszy; wynik.lKolumn = this->lKolumn;

        for(unsigned int i = 0; i < this->elementy.size(); ++i)
        {
            wynik.elementy.push_back(this->elementy[i] + mtx[i]);
        }

        return wynik;
    }
};

/*!
Wypisanie macierzy.
*/
std::ostream& operator << (std::ostream& strumien, const macierz& mtx)
{
    for(unsigned int i = 0; i < mtx.elementy.size() ; ++i)
    {
        strumien << mtx[i] << ' ';
    }

    return strumien;
}

double Timer::nsInterval() const
{
    return std::chrono::duration_cast<std::chrono::nanoseconds>(m_end - m_start)
        .count();
}

double Timer::msInterval() const
{
    const double MS_PER_NS = 1e-6;
    return nsInterval() * MS_PER_NS;
}

class SumTester : public Tester<macierz, std::vector<int>>
{
  protected:
    macierz runAlgorithm(const std::vector<int>& inputData) override;
    std::vector<int> readSingleInput(std::istream& inputStream) override;
};

macierz SumTester::runAlgorithm(const std::vector<int>& inputData)
{
    macierz Mtx1, Mtx2;
    Mtx1.lWierszy = Mtx2.lWierszy = inputData.at(0);
    Mtx1.lKolumn = Mtx2.lKolumn = inputData.at(1);

    for(unsigned int i = 0; i < Mtx1.lWierszy * Mtx1.lKolumn; ++i)
    {
        Mtx1.elementy.push_back(inputData.at(i + 2));
    }
    for(unsigned int i = Mtx2.lWierszy * Mtx2.lKolumn + 2; i < 2 * Mtx2.lWierszy * Mtx2.lKolumn + 2; ++i)
    {
        Mtx2.elementy.push_back(inputData.at(i));
    }

    return Mtx1 + Mtx2;
}

std::vector<int> SumTester::readSingleInput(std::istream& inputStream)
{
    int numOfData1 = 0, numOfData2 = 0;
    std::vector<int> result; result.clear();

    inputStream >> numOfData1;
    result.push_back(numOfData1);
    inputStream >> numOfData2;
    result.push_back(numOfData2);

    for(auto i = 0; i < 2 * numOfData1 * numOfData2; ++i)
    {
        int dataElement;
        inputStream >> dataElement;

        result.push_back(dataElement);
    }

    return result;
}

int main(int argc, char* argv[])
{
    std::ifstream inputFile{"input.txt"};
    std::ofstream dataOutputFile{"output.txt"}, timeOutputFile{"times.csv"};

    SumTester tester;

    if(!inputFile)
    {
        spdlog::error("input.txt cannot be opened!");
        return -1;
    }

    tester.runAllTests(inputFile, dataOutputFile, timeOutputFile);
}
