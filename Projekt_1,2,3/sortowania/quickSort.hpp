#ifndef QUICKSORT
#define QUICKSORT

void swapVectElems (std::vector<int>& vect, int ind1, int ind2)
{
    if(vect.size() <= 1)
        return;

    std::vector<int> helper;

    if(ind1 < ind2)
    {
        for(int i = 0; i < ind1; ++i)
            helper.push_back(vect.at(i) );
        helper.push_back(vect.at(ind2) );
        for(int i = ind1 + 1; i < ind2; ++i)
            helper.push_back(vect.at(i) );
        helper.push_back(vect.at(ind1) );
        for(int i = ind2 + 1; i < vect.size(); ++i)
            helper.push_back(vect.at(i) );
        vect = helper;
    }
    if(ind1 > ind2)
    {
        for(int i = 0; i < ind2; ++i)
            helper.push_back(vect.at(i) );
        helper.push_back(vect.at(ind1) );
        for(int i = ind2 + 1; i < ind1; ++i)
            helper.push_back(vect.at(i) );
        helper.push_back(vect.at(ind2) );
        for(int i = ind1 + 1; i < vect.size(); ++i)
            helper.push_back(vect.at(i) );
        vect = helper;
    }
}

void quickSort (std::vector<int>& vect)
{
    if(vect.size() <= 1)
        return;

    int i = 0, j = 0, pivot = 0;
    i = (vect.size() - 1) / 2;
    pivot = vect.at(i);
    swapVectElems(vect, i, vect.size() - 1);

    for(i = 0; i < vect.size() - 1; ++i)
    {
        if(vect.at(i) < pivot)
        {
            swapVectElems(vect, i, j);
            ++j;
        }
    }

    swapVectElems(vect, j, vect.size() - 1);

    if(j - 1 > 0)
    {
        std::vector<int> tmp;
        for(int k = 0; k < j; ++k)
            tmp.push_back(vect.at(k));

        quickSort(tmp);

        for(int k = 0; k < j; ++k)
            vect.at(k) = tmp.at(k);
    }

    if(j + 1 < vect.size() - 1)
    {
        std::vector<int> tmp;
        for(int k = j + 1; k < vect.size(); ++k)
            tmp.push_back(vect.at(k) );

        quickSort(tmp);

        for(int k = j + 1, l = 0; k < vect.size(); ++k, ++l)
            vect.at(k) = tmp.at(l);
    }
}

#endif // QUICKSORT
