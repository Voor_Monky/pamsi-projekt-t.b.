#ifndef MERGESORT
#define MERGESORT

#include <vector>

/*! Podział wektora na dwa */
void split (const std::vector<int> vect, std::vector<int>& v1, std::vector<int>& v2)
{
    v1.clear(); v2.clear();
    bool switcher = false;

    for(unsigned int i = 0; i < vect.size(); ++i)
    {
        if(switcher)
            v1.push_back(vect.at(i));
        else
            v2.push_back(vect.at(i));

        switcher = !switcher;
    }
}

/*! Scala dwa posortowane wektory. Zwraca posortowany wektor */
std::vector<int> mergeTwo (std::vector<int>& v1, std::vector<int>& v2)
{
    std::vector<int> result;
    unsigned int i = 0, j = 0;

    for(; i < v1.size() && j < v2.size();)
    {
        if(v1.at(i) > v2.at(j))
        {
            result.push_back(v2.at(j));
            ++j;
        }
        else
        {
            result.push_back(v1.at(i));
            ++i;
        }
    }

    for(; i < v1.size(); ++i)
        result.push_back(v1.at(i));

    for(; j < v2.size(); ++j)
        result.push_back(v2.at(j));

    return result;
}

void mergeSort (std::vector<int>& vect)
{
    if(vect.size() > 1)
    {
        std::vector<int> v1, v2;

        split(vect, v1, v2);
        mergeSort(v1);
        mergeSort(v2);
        vect = mergeTwo(v1, v2);
    }
}

#endif // MERGESORT
