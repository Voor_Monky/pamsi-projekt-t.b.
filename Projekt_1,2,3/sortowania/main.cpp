#include <iostream>
#include <fstream>
#include <sstream>

#include "../tester/include/pamsi_tester/Tester.hpp"
#include "randTab.hpp"
#include "mergeSort.hpp"
#include "heapSort.hpp"
#include "quickSort.hpp"

#define TAB_SIZE 50
#define TESTS_NUMBER 10

/*! Wypisywanie wektora */
std::ostream& operator<< (std::ostream& ostr, const std::vector<int> vect)
{
    for(unsigned int i = 0; i < vect.size(); ++i)
    {
        ostr << vect.at(i) << ' ';
    }

    return ostr;
}

using FunType = void (*)(std::vector<int>&);

template <typename ResultType, typename InputType, FunType name>
class SortTester : public Tester<ResultType, InputType>
{
  public:
    virtual ~SortTester() {}

  protected:
    virtual ResultType runAlgorithm(InputType& inputData)
    {
        name(inputData);

        return ResultType{inputData};
    }

    virtual InputType readSingleInput(std::istream& inputStream)
    {
        std::vector<int> input;
        int i;
        std::string tmp;
        std::stringstream tmp2;

        std::getline(inputStream, tmp);
        tmp2 << tmp;

        while(tmp2.good())
        {
            tmp2 >> i;
            input.push_back(i);
        }

        return InputType{input};
    }
};

int main ()
{
    std::ifstream inputFile{"../sortowania/input.txt"};
    std::ofstream dataOutputFile{"../sortowania/output.txt"}, timeOutputFile{"../sortowania/times.csv"};//, mkfile{"../sortowania/input.txt"};

    SortTester<std::vector<int>, std::vector<int>, heapSort> tester;    //3. argument -> Jakie sortowanie?
    //SortTester<std::vector<int>, std::vector<int>, mergeSort> tester;
    //SortTester<std::vector<int>, std::vector<int>, quickSort> tester;

    if(!inputFile)
    {
        spdlog::error("input.txt cannot be opened!");
        return -1;
    }

    tester.runAllTests(inputFile, dataOutputFile, timeOutputFile, 10);

//    std::vector<int> tmp;
//    tmp = makeRandTab(10);
//    std::cout << tmp << std::endl;
//    //heapSort(tmp);
//    //mergesort(tmp);
//    //quickSort(tmp);
//    std::cout << tmp << std::endl;

/* Tworzenie pliku input.txt */
//    std::vector<int> tmp;
//    tmp = makeRandTab(100);
//    mkfile << tmp << std::endl;
//    tmp = makeRandTab(1000);
//    mkfile << tmp << std::endl;;
//    tmp = makeRandTab(10000);
//    mkfile << tmp << std::endl;;
//    tmp = makeRandTab(100000);
//    mkfile << tmp << std::endl;;
//    tmp = makeRandTab(1000000);
//    mkfile << tmp;
}
