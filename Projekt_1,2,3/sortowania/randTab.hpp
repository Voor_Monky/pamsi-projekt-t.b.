#ifndef RANDTAB
#define RANDTAB

#include <ctime>
#include <stdlib.h>
#include <vector>

std::vector<int> makeRandTab(unsigned int rozmiar)
{
    std::vector<int> tab;
    int randomVal;
    std::srand(std::time(0));
    for(unsigned int i = 0; i < rozmiar; ++i)
    {
        randomVal = std::rand();
        tab.push_back(randomVal % 100);
    }

    return tab;
}

#endif // RANDTAB
