#ifndef HEAPSORT
#define HEAPSORT

#include "heapForSort.hpp"

void heapSort(std::vector<int>& tab)
{
    heapForSort heap;
    heap.makeHeap(tab);
    std::vector<int> sortedTab;

    for(unsigned int i = 0; i < tab.size(); ++i)
    {
        sortedTab.push_back(heap.removeMin());
    }

    tab = sortedTab;
}

#endif // HEAPSORT
