#ifndef HEAP_FOR_SORT
#define HEAP_FOR_SORT

#include <climits>

/*! Kopiec minimalny oparty o tablice; indeks korzenia = 1, indeks wezla = i, indeks lewego syna = 2i, indeks prawego syna = 2i+1 */
class heapForSort
{
    std::vector<int> tree;

 public:
     unsigned int lastLeafIndex = 0;

     heapForSort(){this->tree.push_back(INT_MAX);}
     ~heapForSort(){}
     int returnValue(unsigned int) const;
     unsigned int returnFatherIndex(unsigned int) const;
     unsigned int returnLeftSonIndex(unsigned int) const;
     unsigned int returnRightSonIndex(unsigned int) const;
     void switchNodes(unsigned int, unsigned int);
     void upheap(unsigned int);
     void add(int);
     void downheap(unsigned int);
     int removeMin();
     heapForSort makeHeap(const std::vector<int>);
};

int heapForSort::returnValue(unsigned int index) const
{
    if(index > this->tree.size())
        return 0;

    return this->tree.at(index);
}

unsigned int heapForSort::returnFatherIndex(unsigned int index) const
{
    if(index == 1)  //check if root
        return 0;
    else
    {
        if(index%2 == 0)
            return index/2;
        else
            return (index-1)/2;
    }
    return 0;
}

unsigned int heapForSort::returnLeftSonIndex(unsigned int index) const
{
    unsigned int LeSoInd = 2 * index;

    if(LeSoInd >= this->tree.size())
        return 0;
    else
        return LeSoInd;

    return 0;
}

unsigned int heapForSort::returnRightSonIndex(unsigned int index) const
{
    unsigned int RiSoInd = 2 * index + 1;

    if(RiSoInd >= this->tree.size())
        return 0;
    else
        return RiSoInd;

    return 0;
}

void heapForSort::switchNodes(unsigned int index_1, unsigned int index_2)
{
    int elem_1 = this->returnValue(index_1);
    int elem_2 = this->returnValue(index_2);

    this->tree.at(index_1) = elem_2;
    this->tree.at(index_2) = elem_1;
}

void heapForSort::upheap(unsigned int index)
{
    unsigned int fatherIndex = this->returnFatherIndex(index);

    if(fatherIndex == 0)
        return;

    if(this->returnValue(index) < this->returnValue(fatherIndex))
    {
        this->switchNodes(index, fatherIndex);
        this->upheap(fatherIndex);
    }
}

void heapForSort::add(int elem)
{
    this->tree.push_back(elem);
    ++(this->lastLeafIndex);
    this->upheap(this->lastLeafIndex);
}

void heapForSort::downheap(unsigned int index)
{
    unsigned int leftSonInd = this->returnLeftSonIndex(index);
    unsigned int rightSonInd = this->returnRightSonIndex(index);

    if(leftSonInd == 0)
        return;
    if(this->returnValue(leftSonInd) <= this->returnValue(rightSonInd))
    {
        if(this->returnValue(index) > this->returnValue(leftSonInd))
        {
            this->switchNodes(index, leftSonInd);
            this->downheap(leftSonInd);
        }
    }
    else
    {
        if(this->returnValue(index) > this->returnValue(rightSonInd))
        {
            this->switchNodes(index, rightSonInd);
            this->downheap(rightSonInd);
        }
    }
}

int heapForSort::removeMin()
{
    int Min = this->tree.at(1);

    this->switchNodes(1, this->lastLeafIndex);

    this->tree.pop_back();
    this->tree.shrink_to_fit();
    --(this->lastLeafIndex);

    if(this->lastLeafIndex != 0)
        this->downheap(1);

    return Min;
}

heapForSort heapForSort::makeHeap(const std::vector<int> tab)
{
    for(unsigned int i = 0; i < tab.size(); ++i)
    {
        this->add(tab.at(i));
    }

    return *this;
}

#endif // HEAP_FOR_SORT
